/*Теоретичні питання.
1. Метод об'єкту - це коли ми в значення властивості об'єкту записуєм функцію. Наприклад, якщо в нас є об'єкт: const product = {властивості
об'єкта ...price: 2555,}, то ми можемо використовувати метод об'єкта get:
rewPrice: function (){
          return this.price + 'UAH';
          }
 як властивість і значення властивості, щоб нам поверталось значення ціни в гривнях. Функція також може бути стрілковою:
 rewPrice: () => {return this.price + 'UAH'; }
 Ще один способ - створити функцію прямо внутрі об'єкта тим же методом: rewPrice() {return this.price + 'UAH'; }
 Також є методи, які вбудовані в середину об'єкта: get price(){return this._price + 'UAH'; }
 Ми звертаємось до властивості зовні об'єкта, щоб викликати цей метод - product.price = 1050; Також є метод set,
 який змінює значення властивості об'єкта. Також існують інші методи об'єкта.
2. Значення властивості об'єкта може мати будь-який тип данних - рядок, число, масив або інший об'єкт.
3. Об'єкти записуються в змінну та присвоюються за посиланнями. Коли ми створюємо об'єкт, в змінну записується не сам об'єкт, а адреса
осередка пам'яті комп'ютера, в якій зберігається об'єкт.
*/

'use strict'
const product = {
    name: 'Laptop Gigabyte G5 KF',
    price: '40000 UAH',
    discount: '10%',
   get newPrice() {
        return parseInt(this.price) - (parseInt(this.price) * parseInt(this.discount))/ 100;
        },
}
const result = product.newPrice;
console.log(result);


let x = prompt("Введіть своє ім'я");
let y = prompt("Введіть ваш вік");
const user = {};
function greeting(){
    while(isNaN(y)){
       y =  prompt("Введіть будь ласка число");
    }
        user.name = x;
        user.age = y;
    return `Привіт, мені ${user.age} років`;
}
alert(greeting(user.name,user.age));

const shop = {
    dresses: {beach: {size: 'M, L, XL, XXL'},
        evening: {size:'S, XS, M, L, XL'}
    },
    material: 'cotton',
}
function deepClone(){

     if (typeof (shop) === "object"){
        let result = {};
        for (let k in shop){
            result[k] = deepClone(shop[k]);
        }
        return result;
    }
}
deepClone();
